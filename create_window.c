/*	create_window
	Create a new X window on the specified display (must be opened) at the given
	coordinates with the given constraints.

	NO ERROR CHECKING IS DONE.

	Arguments:
	Display *disp - a pointer to the X display
	int max_width, max_height - the maximum pixel width and height of the window
	int width, height - the starting pixel width and height of the window
	int min_width, min_height - the minimum pixel width and height of the window
	int x, y - the starting coordinates of the upper left corner of the window
*/
Window create_window(Display *disp, int max_width, int max_height int width,
	int height, int min_width, int min_height, int x, int y)
{
	int screen;
	Window wind;
	XSizeHints *wind_size_hints;

	screen = DefaultScreen(disp);

	wind = XCreateSimpleWindow(disp, RootWindow(disp, screen),
			x, y, /* the upper left start coordinates */
			width, height, /* the dimensions of the window */
			0, /* the size of window borders - 0 for none */
			BlackPixel(disp, screen), WhitePixel(disp, screen));

	wind_size_hints = XAllocSizeHints();

	wind_size_hints->min_width = min_width;
	wind_size_hints->min_height = min_height;
	wind_size_hints->width = width;
	wind_size_hints->height = height;
	wind_size_hints->max_width = max_width;
	wind_size_hints->max_height = max_height;

	XSetWMNormalHints(disp, wind, wind_size_hints);
	XFree(wind_size_hints);

	XMapWindow(disp, wind);
	XFlush(disp);

	return wind;
}
